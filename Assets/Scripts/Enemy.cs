﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    public float health;
    public float speed;
    public Color color;
    public float size;
    public float damage;
    public Score score;
    public Transform player;
    public CameraRotation cameraRotation;
    private void Awake()
    {
        //setting variables on enemy spawn
        player = GameObject.FindGameObjectWithTag("Player").transform;
        score = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();
        cameraRotation = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraRotation>();

        //changing enemy color according to value from spawner
        Renderer enemyRenderer = GetComponent<Renderer>();
        enemyRenderer.material.color = color;
        //changing scale ccording to value from spawner
        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(scale.x * size, scale.y * size, scale.z * size);
    }

    void Update()
    {
        //setting enemy speed depend of player rotation speed (faster rotation = faster enemy) to pretends a slow motion effect 
        if (cameraRotation.GetAngle() > 2f)
        {
            speed = 10f;
        }
        else if (cameraRotation.GetAngle() > 0.5f)
        {
            speed = 5f;
        }
        else if (cameraRotation.GetAngle() > 0.1f)
        {
            speed = 3f;
        }
        else
        {
            speed = 1.5f;
        }
        //rotating enemy towards a player
        Quaternion enemyRotation = Quaternion.LookRotation(player.position - transform.position);
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, enemyRotation.eulerAngles.y, transform.eulerAngles.z), Time.deltaTime * 1f);
        //moving enemy towards a player
        transform.position = Vector3.MoveTowards(transform.position, player.position,  speed * Time.deltaTime);
    }

    //func used by spawner to create diffrent types of enemies
    public void SetAllVariables(float _health, Color _color, float _size, float _damage)
    {
        health = _health;
        color = _color;
        size = _size;
        damage = _damage;
    }
    //damage got by player from enemy 
    public float GetDamage()
    {
        return damage;
    }
    //detecting collison with bullet
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            Hurt();
        }
    }
    //getting hurt by bullet is equals to get 50 damage by fixed value ( not a variable coz size of the project doesn't need that)
    private void Hurt()
    {
        if (health - 50f > 0)
        {
            health -= 50;
        }
        else
        {
            score.SetScore(damage);
            Destroy(gameObject);
        }
    }
}
