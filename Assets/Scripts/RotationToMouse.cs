﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationToMouse : MonoBehaviour
{
    public Camera cam;
    public float maximumLenght;
    private Ray rayMouse;
    private Vector3 direction;
    private Quaternion rotation;

    void Update()
    {
        if(cam != null)
        {
            RaycastHit hit;
            Vector3 mousePos = Input.mousePosition;
            rayMouse = cam.ScreenPointToRay(mousePos);
            if(Physics.Raycast(rayMouse.origin, rayMouse.direction, out hit, maximumLenght))
            {
                //call function when pointer points on a physical object 
                RotateToMouseDirection(gameObject, hit.point);
            }
            else
            {
                //call function when pointer points towards the air
                Vector3 pos = rayMouse.GetPoint(maximumLenght);
                RotateToMouseDirection(gameObject, pos);

            }
        }
        else
        {
            Debug.Log("No cam");
        }
    }

    void RotateToMouseDirection(GameObject _obj, Vector3 _destination)
    {
        //setting direction towards target object
        direction = _destination - _obj.transform.position;
        rotation = Quaternion.LookRotation(direction);
    }

    //giving a rotation toward a target for bulllet
    public Quaternion GetRotation()
    {
        return rotation;
    }
}
