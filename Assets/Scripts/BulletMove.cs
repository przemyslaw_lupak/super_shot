﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public float bulletSpeed;
    public float fireRate;
    public float damage = 100f;
    //Vizual effects for shoot and hit
    public GameObject muzzlePrefab;
    public GameObject hitPrefab;
    
    public CameraRotation cameraRotation;
   
    private void Awake()
    {
        cameraRotation = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraRotation>();
        
        //spawning muzzle VFX when bullet is created
        if (muzzlePrefab != null)
        {
            GameObject muzzleVFX = Instantiate(muzzlePrefab, transform.position, Quaternion.identity);
            muzzleVFX.transform.forward = gameObject.transform.forward;
            var psMuzzle = muzzleVFX.GetComponent<ParticleSystem>();
            if (psMuzzle != null)
            {
                Destroy(muzzleVFX, psMuzzle.main.duration);
            }
            else
            {
                var psChild = muzzleVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(muzzleVFX, psChild.main.duration);
            }
        }
    }
    void Update()
    {
        //setting bullet speed and fire rate depends of player rotation speed (faster rotation = faster shots) to pretends a slow motion effect 
        if (cameraRotation.GetAngle() > 2f)
        {
            bulletSpeed = 100f;
            fireRate = 4f;
        }
        else
        {
            bulletSpeed = 15f;
            fireRate = 5f;
        }

        //bullet movement
        if (bulletSpeed != 0)
        {
            transform.position += transform.forward * (bulletSpeed * Time.deltaTime);
        }
        else
        {
            Debug.Log("No Speed");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        bulletSpeed = 0;
        //spawning blood VFX if bullet hit an enemy
        if(collision.gameObject.tag == "Enemy") 
        { 
            ContactPoint contact = collision.contacts[0];
            Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
            Vector3 pos = contact.point;
            if (hitPrefab != null)
            {
                GameObject hitVFX = Instantiate(hitPrefab, pos, rot);
                var psHit = hitVFX.GetComponent<ParticleSystem>();
                if (psHit != null)
                {
                    Destroy(hitVFX, psHit.main.duration);
                }
                else
                {
                    var psChild = hitVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                    Destroy(hitVFX, psChild.main.duration);
                }
            }
        }
        Destroy(gameObject);
    }
}
