﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private float timeBeforeStart;
    [SerializeField]
    private float timebtwSpawn;
    //units able to spawn on this wave
    [SerializeField]
    private float waveUnits;
    //checking if wave is spawned or waitng to spawn
    [SerializeField]
    private bool waveSpawned;
    [SerializeField]
    private float timeBetweenWaves = 2f;
    //number of current wave
    [SerializeField]
    private int currentWave = 1;
    //units adds to each future wave
    [SerializeField]
    private int waveUnitsChange = 3;
    
    //spawner borders(sqare)
    public float borderMax = 48;
    public float borderMin = -48;
    //prefab with Enemy script
    public GameObject enemyPrefab;
    // min distance between enemy spawn point and player 
    public float distanceToPlayer = 15f;
    public Transform player;
    void Update()
    {
        //waits before firs wave spawn
        if (timeBeforeStart > 0)
        {
            timeBeforeStart -= Time.deltaTime;
        }
        else
        {
            if (timebtwSpawn > 0)
            {
                timebtwSpawn -= Time.deltaTime;

            }
            else if (timebtwSpawn <= 0)
            {
                //if spawn is off cooldown create new enemy and give him a random cost 1-3
                GameObject newEnemy = enemyPrefab;
                float unityCost = (int)Random.Range(1, 4);
                
                //if random unit cost is able to fit in current wave spawn it
                if (unityCost <= waveUnits)
                {
                    CreateEnemy(unityCost, newEnemy);
                    waveUnits -= unityCost;
                }
                //if random cost enemy cant fit in current wave spawn fixed cost unit
                else if (3 <= waveUnits)
                {
                    CreateEnemy(3, newEnemy);
                    waveUnits -= 3;
                }
                else if (2 <= waveUnits)
                {
                    CreateEnemy(2, newEnemy);
                    waveUnits -= 2;
                }
                else if (1 <= waveUnits)
                {
                    CreateEnemy(1, newEnemy);
                    waveUnits -= 1;
                }
                else if (waveUnits == 0)
                {
                    waveSpawned = true;
                }

            }

        }

        GameObject[] spawnedEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        //waiting unitl all enemy gets destroy then recalculating a streangth and timer for next wave 
        if (spawnedEnemies.Length == 0 && waveSpawned)
        {
            timeBeforeStart = timeBetweenWaves;
            waveSpawned = false;
            currentWave++;
            waveUnits += waveUnitsChange * currentWave;
        }
    }

    void CreateEnemy(float enemyStrength, GameObject newEnemy)
    {
        if (enemyStrength == 1)
        {
            //setting enemys (health, speed, color, scale)
            newEnemy.GetComponent<Enemy>().SetAllVariables(100f, Color.blue, 1f, 10f);
            //random position on the ground
            Vector3 enemyPosition = new Vector3(Random.Range(borderMin, borderMax), 1, Random.Range(borderMin, borderMax));

            //checking if enemy spawn position is in min distance from player
            while (Vector3.Distance(enemyPosition, player.position) < distanceToPlayer)
            {
                //if spawn is to close to player position recalculate position
                enemyPosition = new Vector3(Random.Range(borderMin, borderMax), 1, Random.Range(borderMin, borderMax));
            }
            Instantiate(newEnemy, enemyPosition, Quaternion.identity);
        }
        else if (enemyStrength == 2)
        {
            //setting enemys (health, speed, color, scale)
            newEnemy.GetComponent<Enemy>().SetAllVariables(150f, Color.black, 1.3f, 20f);
            //random position on the ground
            Vector3 enemyPosition = new Vector3(Random.Range(borderMin, borderMax), 1, Random.Range(borderMin, borderMax));

            //checking if enemy spawn position is in min distance from player
            while (Vector3.Distance(enemyPosition, player.position) < distanceToPlayer)
            {
                //if spawn is to close to player position recalculate position
                enemyPosition = new Vector3(Random.Range(borderMin, borderMax), 1, Random.Range(borderMin, borderMax));
            }
            Instantiate(newEnemy, enemyPosition, Quaternion.identity);
        }
        else if (enemyStrength == 3)
        {
            //setting enemys (health, speed, color, scale)
            newEnemy.GetComponent<Enemy>().SetAllVariables(200f, Color.red, 1.5f, 30f);
            //random position on the ground
            Vector3 enemyPosition = new Vector3(Random.Range(borderMin, borderMax), 1, Random.Range(borderMin, borderMax));

            //checking if enemy spawn position is in min distance from player
            while (Vector3.Distance(enemyPosition, player.position) < distanceToPlayer)
            {
                //if spawn is to close to player position recalculate position
                enemyPosition = new Vector3(Random.Range(borderMin, borderMax), 1, Random.Range(borderMin, borderMax));
            }
            Instantiate(newEnemy, enemyPosition, Quaternion.identity);
        }
    }
}
