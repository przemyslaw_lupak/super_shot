﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    //ui to modify
    public Text scoreText;
    //sum of all our points
    public float points;
    //seeting score to always get minimum 4 numbers (with zeros on begging)
    public void SetScore(float _pointsGained)
    {
        points += _pointsGained;
        if(points < 100)
        {
            string score;
            score = "00" + points.ToString();
            scoreText.text = score;
        }
        else if (points < 1000)
        {
            string score;
            score = "0" + points.ToString();
            scoreText.text = score;
        }
        else if (points > 1000)
        {
            string score;
            score = points.ToString();
            scoreText.text = score;
        }
    }
}
