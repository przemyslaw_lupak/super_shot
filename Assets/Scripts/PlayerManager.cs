﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public float health = 100;
    public HealthBar healthBar;
    void Start()
    {
        healthBar.SetMaxHealth(health);
    }

    private void OnTriggerEnter(Collider _other)
    {
        //detecting collisions with enemies
        if(_other.tag == "Enemy")
        {
            Hurt(_other.GetComponent<Enemy>().GetDamage());
            //enemy getting destroyed after collison with player
            Destroy(_other.gameObject);
        }
    }

    private void Hurt(float _damage)
    {
        health -= _damage;
        //seting value of healthbar UI
        healthBar.SetHealth(health);
    }
}
