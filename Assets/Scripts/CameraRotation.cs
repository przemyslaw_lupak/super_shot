﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public Vector3 lastRotation;
    public float angleDelta;
    private void Start()
    {
        lastRotation = transform.forward;
    }
    private void Update()
    {
        angleDelta = Vector3.Angle(lastRotation, transform.forward);
        lastRotation = transform.forward;
    }

    public float GetAngle()
    {
        return angleDelta;
    }
}
