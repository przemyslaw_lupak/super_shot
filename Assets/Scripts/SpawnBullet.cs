﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBullet : MonoBehaviour
{

    //current used visual effect
    private GameObject effectToSpawn;
    private float timeToFire = 1f;

    public GameObject firePoint;
    //list of visual effect used later in script(in case there is more shooting effects or wepons)
    public List<GameObject> vfx = new List<GameObject>();
    public Quaternion cameraRotation;

    public RotationToMouse rotationToMouse;
    void Start()
    {
        //setting current used effect and camera rotation
        effectToSpawn = vfx[0];
        cameraRotation = GetComponentInChildren<Transform>().localRotation;
    }

    void Update()
    {
        //shooting on screen click/hold dependent of fire rate
        if (Input.GetMouseButton(0) && Time.time >= timeToFire)
        {
            timeToFire = Time.time + 1 / effectToSpawn.GetComponent<BulletMove>().fireRate;
            SpawnVFX();
        }
    }

    void SpawnVFX()
    {
        GameObject vfx;
        if (firePoint != null)
        {
            //creating bullet with visual effects
            vfx = Instantiate(effectToSpawn, firePoint.transform.position, Quaternion.identity);
            if (cameraRotation != null)
            {
                //setting rotation of a bullet and shot effects 
                vfx.transform.localRotation = rotationToMouse.GetRotation();
            }
        }
        else
        {
            Debug.Log("No fire Point");
        }
    }
    
}
